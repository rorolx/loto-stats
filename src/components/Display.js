import React from "react"
import PropTypes from "prop-types"

class Display extends React.Component {

 constructor(props){
     super(props);

      this.state = {
        error: null,
        isLoaded: false,
        data: [], 
        years:[],
        yearToYear:[],
        selectedYear: 0,
        selectedYearTable: 0,
    } 
 }
   

    componentDidMount() {
         Promise.all([
            fetch("./data/201902-2020.json").then((response) => response.json()),
            fetch("./data/201609-201902.json").then((response) => response.json()),
            fetch("./data/201402-201609.json").then((response) => response.json()), 
            fetch("./data/201105-201401.json").then((response) => response.json()),
            fetch("./data/200402-201104.json").then((response) => response.json()), 
        ]) 
        .then(
            // result
            data => {
                 
                //all array in one
                const mergeData =  [].concat(...data);

                this.setState({
                    isLoaded: true,
                    data: mergeData
                })  

                this.setState({ 
                    selectedYear: this.getCurrentYear(), 
                    years: this.getYear(),
                    yearToYear: `${this.getYear()[0]}-${this.getYear()[this.getYear().length-1]}`,
                    selectedYearTable: `${this.getYear()[0]}-${this.getYear()[this.getYear().length-1]}`,  
                });  
 
                
                console.log( 'this.state:: ', this.state)
            },
            // error
            error => {
                this.setState({
                    isLoaded: true,
                    error
                }); 
                console.log("error:: ", error)
            }
        )  
    }

 
    /**
     * getYear()
     * from data return unique year annee_numero_de_tirage
     */
    getYear = () => {
    
        const { data } = this.state; 
        const year = []; 

        [].forEach.call( data, (key) => { 

            const str = key.annee_numero_de_tirage.toString().substring(0,4);
            
            if(year.indexOf(str) === -1) year.push(str) ;
        });

        // console.log("getYear:: ", year)

        return year;
    }

    /**
     * getCurrentYear()
     */
    getCurrentYear = () =>{
        const currentYear = this.getYear()[0] ;  
        return currentYear; 
    }
    
    /**
     * getMostDrawed()
     * arg boule || etoile
     * count x
     * return ${type} ${element} ${occurenceNumber} 
     */
    getMostDrawed=( type='boule' )=>{
        
        let element = 0;
        let arrayAllNumbers = []; 
        let occurenceNumber = 0; 
        let numberOfElement = {}; 
        let slug=''; // identifiant boule ou étoile
        const { data, selectedYear, yearToYear } = this.state;   

        if(type==='boule'){
            
            data
            .filter( key => key.annee_numero_de_tirage.toString().startsWith(selectedYear) || selectedYear=== yearToYear)
            .map( key => { 
                    return arrayAllNumbers.push(key.boule_1, key.boule_2, key.boule_3, key.boule_4, key.boule_5) 
             }); 

            slug = 'bowl';
        }

        if(type==='étoile'){ 

            data
            .filter( key => key.annee_numero_de_tirage.toString().startsWith(selectedYear) || selectedYear=== yearToYear)
            .map( key => { 
                   return arrayAllNumbers.push(key.etoile_1, key.etoile_2)  
            });   

            slug = 'star'; 
        }
       
        [].forEach.call( arrayAllNumbers, count => {
            numberOfElement[count] = (numberOfElement[count] || 0) + 1; 
        });
    
        [].forEach.call(arrayAllNumbers, i => {
            if (numberOfElement[i] > occurenceNumber) {
                occurenceNumber = numberOfElement[i];
                    element = i;
            }
        });
        return (`${type} <span class="item item-${slug}"><i class="bg-${slug}"></i>${element}</span> est sortie <strong>${occurenceNumber}</strong> fois`) 
    }
    
    handleChange = (e, name) => { 
        this.setState({[name]: e.target.value});  
    } 

     
    render() { 
        
        const year = this.getYear();

        const { data, yearToYear, selectedYear, selectedYearTable } = this.state;   
        
        return (
            <div className="App-display">
                <h2>Euromillions</h2> 
                <div className="App-render-element">
                <div>
                    <h3>numéro sorti le plus de fois</h3>
                    <table>
                    <thead><tr><td>année</td><td>boule</td><td>étoile</td></tr></thead>
                        <tbody> 
                            <tr>
                                <td>
                                    <select onChange={(e)=> this.handleChange(e, 'selectedYear')} value={ selectedYear }>
                                        <option>{yearToYear}</option>
                                    {  
                                        year.map( (y) => { 
                                           return <option key={y}>{y}</option>
                                        })  
                                    }  
                                        
                                    </select> 
                                </td>
                                <td dangerouslySetInnerHTML={{__html:this.getMostDrawed('boule')}}></td>
                                <td dangerouslySetInnerHTML={{__html:this.getMostDrawed('étoile')}}></td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
                <div>
                <h3>Liste des tirages</h3>
                    <table> 
                        <thead><tr>
                        <td><select onChange={(e)=> this.handleChange(e, 'selectedYearTable')} value={ this.state.selectedYearTable }>
                                 <option>{yearToYear}</option>
                                    {  
                                        year.map( (y) => { 
                                           return <option key={y}>{y}</option>
                                        })  
                                    }  
                            </select>  
                    </td>
                    <td>numéros</td><td>étoiles</td></tr></thead>
                        <tbody>
                            {  
                                data
                                .filter(key => key.annee_numero_de_tirage.toString().startsWith( selectedYearTable ) || selectedYearTable === yearToYear)
                                .map( key => { 
                                              return ( 
                                                <tr className={key.annee_numero_de_tirage} key={key.annee_numero_de_tirage}>
                                                    <td className="date">{key.date_de_tirage}</td>
                                                    <td className="boules">{key.boules_gagnantes_en_ordre_croissant}</td>
                                                    <td className="etoiles">{key.etoiles_gagnantes_en_ordre_croissant}</td>
                                                </tr> 
                                         )  
                                })
                            }
                            </tbody>
                    </table>
                </div>
          </div> 
            </div>
        )
    }

}

export default Display;

Display.propTypes = {
    isLoaded: PropTypes.bool,
    data: PropTypes.array,
    selectedYear: PropTypes.number, 
    years: PropTypes.number,
    yearToYear: PropTypes.string,
    selectedYearTable: PropTypes.string, 
    element:PropTypes.number,
    arrayAllNumbers: PropTypes.array,
    occurenceNumber: PropTypes.number,
    numberOfElement: PropTypes.array, 
    slug: PropTypes.string,
    annee_numero_de_tirage: PropTypes.number,
    date_de_tirage: PropTypes.number,
    boules_gagnantes_en_ordre_croissant: PropTypes.string,
}
